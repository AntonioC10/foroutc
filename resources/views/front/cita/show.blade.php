@extends('layouts.front')

@section('body')
    <hr>
    <div class="container">
        <div class="box cta">
            <nav class="breadcrumb" aria-label="breadcrumbs">
              <ul>
                <li>
                  <a href="#">
                    <span class="icon is-small">
                      <i class="fas fa-home" aria-hidden="true"></i>
                    </span>
                    <span>Bienvenidos</span>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <span class="icon is-small">
                      <i class="fas fa-home" aria-hidden="true"></i>
                    </span>
                    <span>Citas</span>
                  </a>
                </li>
                <li>
                    <a href="#">
                      <span class="icon is-small">
                        <i class="fas fa-home" aria-hidden="true"></i>
                      </span>
                      <span>Citas | {{$tutorPar->name}} </span>
                    </a>
                  </li>
              </ul>
            </nav>
            <hr>
            <p class="has-text-centered">
              <span class="tag is-primary"><i class="fas fa-hand-paper"></i></span> Estás en la sección "Citas", aquí podrás ver a todos nuestros tutores pares para poder agendar una cita, si es que la necesitas.
            </p>
          </div>

          <!-- Comienzo del modal Matricula -->
          <div id="modal-id" class="modal modal-fx-fadeInScale modal-pos-bottom">
                <div class="modal-background"></div>
                <div class="modal-card">

                <div class="card">
                    <header class="card-header">
                    <p class="card-header-title">
                        Solicitud de cita con: {{$tutorPar->name}}
                    </p>
                    <a class="card-header-icon" aria-label="more options">
                        <span class="icon">
                        <i class="fas fa-angle-down" aria-hidden="true"></i>
                        </span>
                    </a>
                    </header>
                    <div class="card-content">
                        <div class="content">

                            {!! Form::open(['route' => 'citas.store', 'method' => 'POST', 'files' => false]) !!}
                                <div class="field is-horizontal">
                                    <div class="field-label">
                                        {!! Form::label('name', 'Motivo: ', ['class' => 'label']) !!}
                                    </div>

                                    <div class="field-body">
                                        <div class="field">
                                            <p class="control">
                                                {!! Form::text('motivo', null, ['class' => 'input', 'placeholder' => 'Necesito ayuda con...', 'required' => 'required']) !!}
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="field is-horizontal">
                                        <div class="field-label">
                                            {!! Form::label('name', 'Asignatura: ', ['class' => 'label']) !!}
                                        </div>

                                        <div class="field-body">
                                            <div class="field">
                                                <p class="control">
                                                    {!! Form::text('asignatura', null, ['class' => 'input', 'placeholder' => 'Necesito ayuda en...', 'required' => 'required']) !!}
                                                </p>
                                            </div>
                                        </div>
                                    </div>

                                <div class="field is-horizontal">
                                    <div class="field-label">
                                        {!! Form::label('name', 'Fecha cita: ', ['class' => 'label']) !!}
                                    </div>

                                    <div class="field-body">
                                        <div class="field">
                                            <p class="control">
                                                {!! Form::date('fecha', null,['class' => 'form-control']) !!}
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="field is-horizontal">
                                    {!! Form::hidden('users_id', Auth::user()->id, ['class' => 'hidden']) !!}
                                </div>
                                <hr>
                                {!! Form::submit('Solicitar', ['class' => 'button is-block is-large is-primary', 'style' => 'float:right;']) !!}

                            {!! Form::close() !!}

                        </div>
                    </div>

                </div>

                </div>
            </div>
         <!-- Final del modal Matricula -->

            <div class="box cta">
              <div class="row columns center">
                <div class="column is-three-quarters">
                    <div class="card large">
                        <div class="card-content">
                            <div class="media">
                                <div class="media-left">
                                    <figure class="image is-128x128">
                                        <img src="../../images/{{$tutorPar->image}}" alt="Image">
                                    </figure>
                                </div>
                                <div class="media-content center">
                                    <p class="title is-4 no-padding">{{$tutorPar->name}}</p>
                                </div>
                            </div>
                            <br>
                            <hr>
                            <div class="content">
                                <div class="media-content">
                                    <p><span class="title is-5">Carrera: </span>{{$tutorParCita->nombreCarrera}}</p>
                                    <p><span class="title is-5">Grupo: </span>{{$tutorPar->grupoAlumno}}</p>
                                    <p><span class="title is-5">Cuatrimestre: </span>{{$tutorPar->cuatrimestreAlumno}}</p>
                                    <p><span class="title is-5">División: </span>{{$tutorPar->divisionAlumno}}</p>
                                </div>
                                <hr>
                                <p><span class="title is-5 center">¿Quién Soy? </span></p>
                                {{$tutorPar->soyAlumno}}
                                <div class="background-icon"><span class="icon-twitter"></span></div>
                                <hr>

                                <table class="table is-bordered is-striped is-narrow is-hoverable is-fullwidth">
                                        <thead>
                                          <tr>
                                            <th><abbr title="Conocimientos del tutor par.">Conocimientos</abbr></th>
                                            <th><abbr title="Habilidades del tutor par.">Habilidades</abbr></th>
                                            <th><abbr title="Actitudes del tutor par">Actitudes</abbr></th>
                                          </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>{{$tutorPar->conAlumno1}}</td>
                                                <td>{{$tutorPar->habAlumno1}}</td>
                                                <td>{{$tutorPar->actAlumno1}}</td>
                                            </tr>
                                            <tr>
                                                <td>{{$tutorPar->conAlumno2}}</td>
                                                <td>{{$tutorPar->habAlumno2}}</td>
                                                <td>{{$tutorPar->actAlumno2}}</td>
                                            </tr>
                                            <tr>
                                                <td>{{$tutorPar->conAlumno3}}</td>
                                                <td>{{$tutorPar->habAlumno3}}</td>
                                                <td>{{$tutorPar->actAlumno3}}</td>
                                            </tr>
                                            <tr>
                                                <td>{{$tutorPar->conAlumno4}}</td>
                                                <td>{{$tutorPar->habAlumno4}}</td>
                                                <td>{{$tutorPar->actAlumno4}}</td>
                                            </tr>
                                        </tbody>
                                      </table>

                            </div>
                            <hr>
                            <span class="button is-link is-block is-alt is-fullwidth modal-button" data-target="modal-id">Solicitar Cita</span>
                        </div>
                    </div>
                </div>
              </div>
            </div>
    </div>
@endsection
