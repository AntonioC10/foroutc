@extends('layouts.front')

@section('body')


<hr>
<div class="container">


  <div class="box cta">
    <nav class="breadcrumb" aria-label="breadcrumbs">
      <ul>
        <li>
          <a href="#">
            <span class="icon is-small">
              <i class="fas fa-home" aria-hidden="true"></i>
            </span>
            <span>Bienvenidos</span>
          </a>
        </li>
      </ul>
    </nav>
    <hr>
    <p class="has-text-centered">
      <span class="tag is-primary">New</span> Here we have modal cards. When you click on them they will open functional modal examples.
    </p>
  </div>


</div>


@endsection
