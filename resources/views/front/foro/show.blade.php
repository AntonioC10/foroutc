@extends('layouts.front')

@section('body')

    <hr>
    <div class="container">

        @if(session('status'))
        <article class="message is-success">
            <div class="message-header">
            <p>Error</p>
            <button class="delete" aria-label="delete"></button>
            </div>
            <div class="message-body">
            {{ session('status') }}
            </div>
        </article>
        @endif

        <div class="box cta">
            <nav class="breadcrumb" aria-label="breadcrumbs">
                <ul>
                <li>
                    <a href="#">
                    <span class="icon is-small">
                        <i class="fas fa-home" aria-hidden="true"></i>
                    </span>
                    <span>Bienvenidos</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                    <span class="icon is-small">
                        <i class="fas fa-align-justify" aria-hidden="true"></i>
                    </span>
                    <span>Foros</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                    <span class="icon is-small">
                        <i class="fas fa-align-justify" aria-hidden="true"></i>
                    </span>
                    <span>{{$foroDetalle->temaHilo}}</span>
                    </a>
                </li>
                </ul>
            </nav>
            <hr>
            <p class="has-text-centered">
                <span class="tag is-primary"><i class="fas fa-hand-paper"></i></span> Estás en la sección "Foro", aquí podrás hacer preguntas y ayudar a tus compañeros en sus dudas.
            </p>
        </div>

        <div class="box content">

        <article class="media">
            <figure class="media-left">
                <p class="image is-64x64">
                <img src="../../images/{{$foroDetalle->image}}">
                </p>
            </figure>
            <div class="media-content">
                <div class="content">
                    <p>
                        <strong>{{$foroDetalle->name}}</strong>
                        <br>
                            {{$foroDetalle->contenidoHilo}}
                        <br>
                        <figure class="image">
                            <img src="../../images/{{$foroDetalle->anexoHilo}}">
                        </figure>
                    </p>
                </div>
                @foreach($comentario as $comentarios)
                <article class="media">
                    <figure class="media-left">
                        <p class="image is-48x48">
                        <img src="../../images/{{$comentarios->image}}">
                        </p>
                    </figure>
                    @if($comentarios->anexoComentario == "NoImagen" || $comentarios->anexoComentario == "NoFoto")
                    <div class="media-content">
                        <div class="content">
                        <p>
                            <strong>{{$comentarios->name}}</strong>
                            <br>
                                {{$comentarios->contenidoComentario}}
                            <br>
                        </p>
                    </div>
                    @else
                    <div class="media-content">
                        <div class="content">
                        <p>
                            <strong>{{$comentarios->name}}</strong>
                            <br>
                                {{$comentarios->contenidoComentario}}
                            <br>
                            <figure class="image">
                                <img src="../../images/{{$comentarios->anexoComentario}}">
                            </figure>
                        </p>
                    </div>
                    @endif
                </article>
                @endforeach
            </div>
        </article>

        <article class="media">
            <figure class="media-left">
                <p class="image is-64x64">
                <img src="../../images/{{Auth::user()->image}}">
                </p>
            </figure>
            <div class="media-content">
            {!! Form::open(['route' => 'comentarios.store', 'method' => 'POST', 'files' => true]) !!}
                <div class="field">
                    <p class="control">
                        {!! Form::textarea('contenidoComentario', null, ['class' => 'input', 'placeholder' => 'Deja tu comentario...', 'required' => 'required']) !!}
                    </p>
                </div>
                    {!! Form::file('image', null, ['class' => 'file-input']) !!}

                    {!! Form::hidden('idHilo', $foroDetalle->idHilo, ['class' => 'hidden']) !!}
                    {!! Form::hidden('users_id', Auth::user()->id, ['class' => 'hidden']) !!}
                    
                <div class="field">
                    <p class="control">
                        {!! Form::submit('Postear comentario', ['class' => 'button is-block is-large is-primary', 'style' => 'float:right;']) !!}
                    </p>
                </div>
            </div>
            {!! Form::close() !!}
        </article>
    </div>

    </div>

@endsection