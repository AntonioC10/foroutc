<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Rutas Login / Logout
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::post('login', 'Auth\LoginController@login')->name('login');
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout')->name('logout');

//Rutas FrontEnd
Route::get('/', function () {
    return view('front.bienvenido');
})->name('home');

Route::get('/info-depto', function () {
    return view('front.infodepto');
});

Route::get('/report-problema', function () {
    return view('front.reportproblema');
});

Route::get('/contact', function () {
    return view('front.contact');
});

Route::get('/login', function() {
  return view('auth.login');
});

Route::get('register/{id}', 'APIController@show');
Route::get('registerPar/{id}', 'APIController@showPar');


Route::resource('foros','ForoController');
Route::resource('comentarios','ComentarioController');
Route::resource('citas','CitaController');



//Rutas BackEnd
Route::resource('tutorados','TutoradoController');
Route::resource('tutoresPar','TutorParController');
