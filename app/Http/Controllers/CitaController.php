<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Cita;


class CitaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tutorParCita = User::select('users.id', 'users.name', 'users.email', 'carreras.nombreCarrera', 'users.grupoAlumno', 'users.cuatrimestreAlumno', 'users.sexoAlumno', 'users.divisionAlumno', 'users.image', 'users.soyAlumno')
        ->join('carreras', 'users.carreras_idCarrera', '=', 'carreras.idCarrera')
        ->where('rolUser', 'tutorPar')
        ->get();

        return view('front.cita.index', ['tutorParCita'=>$tutorParCita]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $cita = new Cita;

        $cita->motivoCita = $request->motivo;
        $cita->asignaturaCita = $request->asignatura;
        $cita->fechaCita = $request->fecha;
        $cita->users_id = $request->users_id;
        $cita->save();

        return redirect()->route('citas.index')->with('status', 'Se ha apartado correctamente tu cita :).');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tutorPar = User::Find($id);

        $tutorParCita = User::select('carreras.nombreCarrera')
        ->join('carreras', 'users.carreras_idCarrera', '=', 'carreras.idCarrera')
        ->where('rolUser', 'tutorPar')
        ->where('users.id', $id)
        ->first();

        return view('front.cita.show', compact('tutorPar', 'tutorParCita'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
