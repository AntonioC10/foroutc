@extends('layouts.front')

@section('bodyButton')
  <span class="button modal-button" data-target="modal-id">Registrate</span>
@endsection

@section('body')
  <hr>
  <div class="container">

    @if(session('status'))
      <article class="message is-danger">
        <div class="message-header">
          <p>Error</p>
          <button class="delete" aria-label="delete"></button>
        </div>
        <div class="message-body">
          {{ session('status') }}
        </div>
      </article>
    @elseif(Session::has('error'))
    <article class="message is-danger">
        <div class="message-header">
          <p>Error</p>
          <button class="delete" aria-label="delete"></button>
        </div>
        <div class="message-body">
          {{ Session::get('error') }}
        </div>
      </article>
    @elseif(Session::has('success'))
    <article class="message is-success">
      <div class="message-header">
        <p>Error</p>
        <button class="delete" aria-label="delete"></button>
      </div>
      <div class="message-body">
        {{ Session::get('success') }}
      </div>
    </article>
    @endif

    <div class="box cta">
      <nav class="breadcrumb" aria-label="breadcrumbs">
        <ul>
          <li>
            <a href="/">
              <span class="icon is-small">
                <i class="fas fa-home" aria-hidden="true"></i>
              </span>
              <span>Bienvenidos</span>
            </a>
          </li>
        </ul>
      </nav>
      <hr>
      <p class="has-text-centered">
        <span class="tag is-primary"><i class="fas fa-hand-paper"></i></span> Bienvenido a la plataforma de Tutoría entre Pares.
      </p>
    </div>

    <!-- Comienzo del modal Matricula -->
    <div id="modal-id" class="modal modal-fx-fadeInScale modal-pos-bottom">
        <div class="modal-background"></div>
        <div class="modal-content is-tiny">

          <div class="card">
            <header class="card-header">
              <p class="card-header-title">
                Verificación de Matricula
              </p>
              <a class="card-header-icon" aria-label="more options">
                <span class="icon">
                  <i class="fas fa-angle-down" aria-hidden="true"></i>
                </span>
              </a>
            </header>
            <div class="card-content">
              <div class="content">
                Para poder registrarte, necesitas ser un alumno activo de la Universidad Tecnológica de Cancún.
                <hr>
                <div class="field is-horizontal">
                    <div class="field-label">
                        <label class="label">Matricula</label>
                    </div>

                    <div class="field-body">
                        <div class="field">
                            <p class="control">
                                <input class="input" id="matricula" type="number" name="matricula" value="15391009" placeholder="15391009"
                                       required autofocus>
                            </p>

                            @if ($errors->has('matricula'))
                              <p class="help is-danger">
                                  {{ $errors->first('matricula') }}
                              </p>
                            @endif
                        </div>
                    </div>
                </div>
              </div>
            </div>
            <footer class="card-footer">
              <input type="button" value="Verificar" class="button is-fullwidth is-primary" onclick="location.href='/register/'+ document.getElementById('matricula').value;">
            </footer>
          </div>

        </div>
    </div>
    <!-- Final del modal Matricula -->


    <div class="tile is-ancestor">
      <div class="tile is-parent">
        <article class="tile is-child box">
          <p class="title">Tutoría en pares <span class="tag is-link is-normal"><i class="fas fa-users"></i></span> </p>
          <p class="subtitle">¿Qué es esto?</p>
          <div class="content">
            <p style="text-align: justify">Es la particularidad de fomentar aprendizajes en forma bidireccional, en donde estudiantes son parte activa del proceso por aspectos socio-afectivos y cognitivos, el uso de códigos comunes, edades, comunicación, lo cual permite mayor disposición ante el aprendizaje ya que entre pares pueden identificarse. Este tipo de acompañamiento impacta en mayores niveles de autonomía por ambas partes, tanto en el “estudiante experto” como en el “estudiante asistido”, la Zona de Desarrollo Próximo permite que las habilidades y conductas estén en constante cambio, lo que hoy exige un máximo de apoyo y asistencia, mañana necesitará un mínimo de ayuda. </p>
          </div>
          <figure class="image is-square">
            <img src="https://itculiacan.edu.mx/wp-content/uploads/2015/09/TEC-52.jpg">
          </figure>
          <hr>
        </article>
      </div>

      <div class="tile is-parent">
        <article class="tile is-child box">
          <p class="title">Tutor Par <span class="tag is-link is-normal"><i class="fas fa-user-graduate"></i></span> </p>
          <p class="subtitle">¿Quiénes son?</p>
          <div class="content">
            <p style="text-align: justify">Estudiantes de Licenciatura e Ingeniería que asesora académicamente a estudiantes del nivel TSU.<br></p>

            <ol type="1">
              <li><b>Ambito:</b> Académico.</li>
              <li><b>Rol:</b> Orientación.</li>
              <li><b>Actividades:</b> Orientar a estudiantes de nuevo ingreso sobre el Programa de Estudios (Plan de Estudios y Programas de las asignaturas)</li>
            </ol>
          </div>
          <figure class="image is-square">
            <img src="https://teenlife.s3.amazonaws.com/media/uploads/blogs/seven-questions-to-ask-before-you-hire-that-sat-or-act-tutor/tutoring%20ques.jpg">
          </figure>
          <hr>
        </article>
      </div>

      <div class="tile is-parent">
        <article class="tile is-child box">
          <p class="title">Tutorados <span class="tag is-link is-normal"><i class="fas fa-book-reader"></i></span> </p>
          <p class="subtitle">¿Por qué?</p>
          <div class="content">
          <div class="content">
            <p style="text-align: justify">Cualquier estudiante de la Universidad Tecnológica de Cancún del nivel Técnico Superior Universitario que requieran apoyo en el ámbito académico y solicite acompañamiento en su proceso formativo con un Tutor Par.</p>
            </div>
        </div>
          <figure class="image is-square">
            <img src="https://image.freepik.com/foto-gratis/estudiante-universitario-libro-al-aire-libre_11055-645.jpg">
          </figure>
          <hr>
        </article>
    </div>
  </div>
  
</div>
@endsection
