<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class APIController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $client = new Client([ 'base_uri' => 'http://siu.utcancun.edu.mx/api/AlumnosApi/GetAlumnosByMatricula', ]);

      $response = $client->request('Get', "?matricula={$id}");

      $data = json_decode($response->getBody()->getContents());

      if($data != false){
        return view('auth.register', compact('data'));
      }else{
        return redirect()->route('home')->with('status', 'La matricula no existe.');
      }

    }

    public function showPar($id)
    {
      $client = new Client([ 'base_uri' => 'http://siu.utcancun.edu.mx/api/AlumnosApi/GetAlumnosByMatricula', ]);

      $response = $client->request('Get', "?matricula={$id}");

      $data = json_decode($response->getBody()->getContents());

      if($data != false){
        return view('auth.registerPar', compact('data'));
      }else{
        return redirect()->route('home')->with('status', 'La matricula no existe.');
      }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
