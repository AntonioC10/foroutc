-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema forout
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema forout
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `forout` DEFAULT CHARACTER SET utf8mb4 ;
USE `forout` ;

-- -----------------------------------------------------
-- Table `forout`.`carreras`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `forout`.`carreras` (
  `idCarrera` INT(11) NOT NULL AUTO_INCREMENT,
  `nombreCarrera` VARCHAR(255) NOT NULL,
  `claveSaeCarrera` INT(11) NOT NULL,
  `divisionCarrera` VARCHAR(255) NOT NULL,
  `estatusCarrera` INT(11) NOT NULL,
  `idSucesorCarrera` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`idCarrera`),
  UNIQUE INDEX `claveSaeCarrera_UNIQUE` (`claveSaeCarrera` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;



-- -----------------------------------------------------
-- Table `forout`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `forout`.`users` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `email` VARCHAR(255) NOT NULL,
  `email_verified_at` TIMESTAMP NULL DEFAULT NULL,
  `password` VARCHAR(255) NOT NULL,
  `remember_token` VARCHAR(100) NULL DEFAULT NULL,
  `created_at` TIMESTAMP NULL DEFAULT NULL,
  `updated_at` TIMESTAMP NULL DEFAULT NULL,
  `nombreAlumno` VARCHAR(255) NULL DEFAULT NULL,
  `matriculaAlumno` VARCHAR(255) NULL DEFAULT NULL,
  `carreras_idCarrera` INT(11) NULL DEFAULT NULL,
  `curpAlumno` VARCHAR(255) NULL DEFAULT NULL,
  `grupoAlumno` VARCHAR(255) NULL DEFAULT NULL,
  `cuatrimestreAlumno` VARCHAR(255) NULL DEFAULT NULL,
  `fechaNacAlumno` VARCHAR(255) NULL DEFAULT NULL,
  `sexoAlumno` VARCHAR(45) NULL DEFAULT NULL,
  `divisionAlumno` VARCHAR(255) NULL DEFAULT NULL,
  `rolUser` VARCHAR(255) NULL DEFAULT NULL,
  `image` VARCHAR(255) NULL DEFAULT NULL,
  `soyAlumno` TEXT NULL DEFAULT NULL,
  `conAlumno1` TEXT NULL DEFAULT NULL,
  `conAlumno2` TEXT NULL DEFAULT NULL,
  `conAlumno3` TEXT NULL DEFAULT NULL,
  `habAlumno1` TEXT NULL DEFAULT NULL,
  `habAlumno2` TEXT NULL DEFAULT NULL,
  `habAlumno3` TEXT NULL DEFAULT NULL,
  `habAlumno4` TEXT NULL DEFAULT NULL,
  `actAlumno1` TEXT NULL DEFAULT NULL,
  `actAlumno2` TEXT NULL DEFAULT NULL,
  `actAlumno3` TEXT NULL DEFAULT NULL,
  `actAlumno4` TEXT NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `users_email_unique` (`email` ASC),
  UNIQUE INDEX `matriculaAlumno_UNIQUE` (`matriculaAlumno` ASC),
  INDEX `fk_users_carreras1_idx` (`carreras_idCarrera` ASC),
  CONSTRAINT `fk_users_carreras1`
    FOREIGN KEY (`carreras_idCarrera`)
    REFERENCES `forout`.`carreras` (`idCarrera`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci;


-- -----------------------------------------------------
-- Table `forout`.`citas`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `forout`.`citas` (
  `idCita` INT(11) NOT NULL AUTO_INCREMENT,
  `motivoCita` VARCHAR(255) NULL,
  `asignaturaCita` VARCHAR(255) NULL,
  `fechaCita` VARCHAR(45) NULL,
  `users_id` INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`idCita`),
  INDEX `fk_citas_users1_idx` (`users_id` ASC),
  CONSTRAINT `fk_citas_users1`
    FOREIGN KEY (`users_id`)
    REFERENCES `forout`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `forout`.`hilos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `forout`.`hilos` (
  `idHilo` INT(11) NOT NULL AUTO_INCREMENT,
  `temaHilo` VARCHAR(255) NOT NULL,
  `contenidoHilo` TEXT NOT NULL,
  `anexoHilo` VARCHAR(255) NOT NULL,
  `users_id` INT(10) UNSIGNED NOT NULL,
  `created_at` TIMESTAMP NULL DEFAULT NULL,
  PRIMARY KEY (`idHilo`),
  INDEX `fk_hilos_users_idx` (`users_id` ASC),
  CONSTRAINT `fk_hilos_users`
    FOREIGN KEY (`users_id`)
    REFERENCES `forout`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `forout`.`comentarios`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `forout`.`comentarios` (
  `idComentario` INT(11) NOT NULL AUTO_INCREMENT,
  `contenidoComentario` TEXT NOT NULL,
  `anexoComentario` VARCHAR(255) NOT NULL,
  `hilos_idHilo` INT(11) NOT NULL,
  `users_id` INT(10) UNSIGNED NOT NULL,
  `created_at` TIMESTAMP NULL DEFAULT NULL,
  PRIMARY KEY (`idComentario`),
  INDEX `fk_comentarios_hilos1_idx` (`hilos_idHilo` ASC),
  INDEX `fk_comentarios_users1_idx` (`users_id` ASC),
  CONSTRAINT `fk_comentarios_hilos1`
    FOREIGN KEY (`hilos_idHilo`)
    REFERENCES `forout`.`hilos` (`idHilo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_comentarios_users1`
    FOREIGN KEY (`users_id`)
    REFERENCES `forout`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `forout`.`materias`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `forout`.`materias` (
  `idMateria` INT(11) NOT NULL,
  `nombreMateria` VARCHAR(255) NOT NULL,
  `cuatrimestreMateria` INT(11) NULL DEFAULT NULL,
  `carreras_idCarrera` INT(11) NOT NULL,
  PRIMARY KEY (`idMateria`),
  INDEX `fk_materias_carreras1_idx` (`carreras_idCarrera` ASC),
  CONSTRAINT `fk_materias_carreras1`
    FOREIGN KEY (`carreras_idCarrera`)
    REFERENCES `forout`.`carreras` (`idCarrera`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `forout`.`migrations`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `forout`.`migrations` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` VARCHAR(255) NOT NULL,
  `batch` INT(11) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci;


-- -----------------------------------------------------
-- Table `forout`.`password_resets`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `forout`.`password_resets` (
  `email` VARCHAR(255) NOT NULL,
  `token` VARCHAR(255) NOT NULL,
  `created_at` TIMESTAMP NULL DEFAULT NULL,
  INDEX `password_resets_email_index` (`email` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
