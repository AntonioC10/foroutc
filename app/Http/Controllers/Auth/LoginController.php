<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use Illuminate\Http\Request;

class LoginController extends Controller
{
  public function login(){
    $credentials = $this->validate(request(), [
      'email' => 'email | required | string',
      'password' => 'required | string'
    ]);

    if(Auth::attempt($credentials)){
      if (Auth::user()->rolUser=='tutorado'){
        return view('front.bienvenido');
      } elseif(Auth::user()->rolUser=='tutorpar') {
        return view('front.bienvenido');
      }elseif(Auth::user()->rolUser=='admin'){
        return view('back.home');
      }
    }

    return back()->withErrors(['email' => 'Estas credenciales no concuerdan con nuestros registros']);
    }

    public function logout(Request $request) {
      Auth::logout();
      return view('front.bienvenido');
    }

}
