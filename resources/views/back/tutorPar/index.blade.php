@extends('layouts.back')

@section('body')


<hr>
<div class="container">



    <div class="columns">

                <div class="column is-3 ">
                    <aside class="menu is-hidden-mobile">
                        <p class="menu-label">
                            General
                        </p>
                        <ul class="menu-list">
                            <li><a class="menu-label" href="/home">Dashboard</a></li>
                        </ul>
                        <p class="menu-label">
                            Administración
                        </p>
                        <ul class="menu-list">
                            <li>
                                <a>Administración de usuarios</a>
                                <ul>
                                    <li><a href="/tutorados">Tutorados</a></li>
                                    <li><a class="is-active" href="/tutoresPar">Tutores Par</a></li>
                                    <li><a>Administradores</a></li>
                                    <li><a>Carreras</a></li>
                                </ul>
                            </li>
                            <li><a>Citas</a></li>
                            <li><a>Foros</a></li>
                        </ul>
                        <p class="menu-label">
                            Otros
                        </p>
                        <ul class="menu-list">
                            <li><a>Reporte</a></li>
                            <li><a>Reporte</a></li>
                            <li><a>Reporte</a></li>
                        </ul>
                    </aside>
                </div>

                <div class="column is-9">
                @if(session('status'))
                <article class="message is-danger">
                  <div class="message-header">
                    <p>Error</p>
                    <button class="delete" aria-label="delete"></button>
                  </div>
                  <div class="message-body">
                    {{ session('status') }}
                  </div>
                </article>
                @endif
                  <div class="box cta">
                    <nav class="breadcrumb" aria-label="breadcrumbs">
                      <ul>
                        <li>
                          <a href="#">
                            <span class="icon is-small">
                              <i class="fas fa-users-cog"></i>
                            </span>
                            <span>Administración</span>
                          </a>
                        </li>
                        <li>
                          <a href="#">
                            <span class="icon is-small">
                              <i class="fas fa-user-graduate"></i>
                            </span>
                            <span>Gestión de tutores Par</span>
                          </a>
                        </li>
                      </ul>
                    </nav>
                  </div>

                    <!-- Comienzo del modal Matricula -->
    <div id="modal-id" class="modal modal-fx-fadeInScale modal-pos-bottom">
        <div class="modal-background"></div>
        <div class="modal-content is-tiny">

          <div class="card">
            <header class="card-header">
              <p class="card-header-title">
                Verificación de Matricula
              </p>
              <a class="card-header-icon" aria-label="more options">
                <span class="icon">
                  <i class="fas fa-angle-down" aria-hidden="true"></i>
                </span>
              </a>
            </header>
            <div class="card-content">
              <div class="content">
                Para poder registrarte, necesitas ser un alumno activo de la Universidad Tecnológica de Cancún.
                <hr>
                <div class="field is-horizontal">
                    <div class="field-label">
                        <label class="label">Matricula</label>
                    </div>

                    <div class="field-body">
                        <div class="field">
                            <p class="control">
                                <input class="input" id="matricula" type="number" name="matricula" value="15391009" placeholder="15391009"
                                       required autofocus>
                            </p>

                            @if ($errors->has('matricula'))
                              <p class="help is-danger">
                                  {{ $errors->first('matricula') }}
                              </p>
                            @endif
                        </div>
                    </div>
                </div>
              </div>
            </div>
            <footer class="card-footer">
              <input type="button" value="Verificar" class="button is-fullwidth is-primary" onclick="location.href='/registerPar/'+ document.getElementById('matricula').value;">
            </footer>
          </div>

        </div>
    </div>
    <!-- Final del modal Matricula -->

                    <section class="info-tiles">
                        <div class="tile is-ancestor has-text-centered">
                            <div class="tile is-parent">
                                <article class="tile is-child box">
                                  <span class="button is-link is-focused is-fullwidth modal-button" data-target="modal-id">Registrar Nuevo Tutor Par</span>
                                </article>
                            </div>
                            <div class="tile is-parent">
                                <article class="tile is-child box">
                                  <a class="button is-primary is-focused is-fullwidth">Importar Archivo CSV</a>
                                </article>
                            </div>
                        </div>
                    </section>

                    <hr>
                    <div class="box cta">
                    <table class="table">
                      <thead>
                        <tr>
                          <th><abbr title="ID">ID</abbr></th>
                          <th>Nombre</th>
                          <th><abbr title="Correo">Correo</abbr></th>
                          <th><abbr title="Carrera">Carrera</abbr></th>
                          <th><abbr title="Grupo">Grupo</abbr></th>
                          <th><abbr title="Cuatrimestre">Cuatrimestre</abbr></th>
                          <th><abbr title="Sexo">Sexo</abbr></th>
                          <th><abbr title="Foto">Foto</abbr></th>
                        </tr>
                      </thead>
                      <tbody>
                      @foreach($tutorPar as $tutorPars)
                        <tr>
                          
                          <th>{{ $tutorPars->id }}</th>
                          <td>{{ $tutorPars->name }}</td>
                          <td>{{ $tutorPars->email }}</td>
                          <td>{{ $tutorPars->nombreCarrera }}</td>
                          <td>{{ $tutorPars->grupoAlumno }}</td>
                          <td>{{ $tutorPar->cuatrimestreAlumno }}</td>
                          <td>{{ $tutorPars->sexoAlumno }}</td>
                          <td>
                            <figure class="image is-128x128">
                              <img src="../../images/{{ $tutorados->image }}">
                            </figure>
                          </td>
                         
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                    </div>

                </div>
            </div>


</div>


@endsection
