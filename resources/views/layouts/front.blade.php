<!DOCTYPE html>
<html class="has-navbar-fixed-top">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Tutoría entre pares</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.4/css/bulma.min.css">
    <link rel="stylesheet" href="https://unpkg.com/bulma-modal-fx/dist/css/modal-fx.min.css" />
    <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
    <style>
        .center {
            display: flex;
            justify-content: center;
            align-items: center;
        }
    </style>
  </head>
  <body>

    <!-- NavBar -->
    <nav class="navbar is-fixed-top is-primary is-flex" role="navigation" aria-label="main navigation">
      <div class="navbar-brand">
        <a class="navbar-item" href="{{ url('/') }}">

          <img src="http://utcancun.edu.mx/wp-content/uploads/2016/06/1200px-LogoBIS-01.png" style="width: 190px; height: 90px;">

        </a>

        <a role="button" class="navbar-burger burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
        <span aria-hidden="true"></span>
        <span aria-hidden="true"></span>
        <span aria-hidden="true"></span>
        <span aria-hidden="true"></span>
        <span aria-hidden="true"></span>
        <span aria-hidden="true"></span>
        <span aria-hidden="true"></span>
        <span aria-hidden="true"></span>
        </a>
      </div>

      <div id="navbarBasicExample" class="navbar-menu">
        @guest
          <div class="navbar-start">
          <a href="{{ url('/') }}" class="navbar-item is-size-6">
            <i class="fas fa-home"></i> | Bienvenidos
          </a>
          <a href="/citas" class="navbar-item is-size-6">
            <i class="fas fa-dice-two"></i> | Citas
            </a>
          <div class="navbar-item has-dropdown is-hoverable is-size-6">
          <a class="navbar-link">
            <i class="fas fa-question"></i> | ¿Quiénes somos?
          </a>
          <div class="navbar-dropdown">
            <a href="{{ url('/info-depto') }}" class="navbar-item is-size-6">
              Departamento de tutoría y desarrollo estudiantíl
            </a>
            <hr class="navbar-divider">
            <a href="{{ url('/report-problema') }}" class="navbar-item is-size-6">
              Reportar un problema
            </a>
          </div>
          </div>
          <a href="{{ url('/contact') }}" class="navbar-item is-size-6">
          <i class="fas fa-at"></i> | Contactanos
          </a>
          </div>

          <div class="navbar-end">
          <div class="navbar-item">
          <div class="buttons">
            @yield('bodyButton')
            <!-- <a class="button is-primary" href="{{ route('register') }}">
              <strong>Regístrate</strong> -->
            </a>
            <a class="button is-info" href="{{ route('login') }}">
              Iniciar Sesión
            </a>
          </div>
          </div>
          </div>
        @else
          <div class="navbar-start">
            <a href="{{ url('/') }}" class="navbar-item is-size-6">
              <i class="fas fa-home"></i> | Bienvenidos
            </a>
            <a href="/foros" class="navbar-item is-size-6">
            <i class="fas fa-align-justify"></i> | Foros
            </a>

            <a href="/citas" class="navbar-item is-size-6">
            <i class="fas fa-dice-two"></i> | Citas
            </a>

            <a href="{{ url('Administracion') }}" class="navbar-item is-size-6">
            <i class="fas fa-chalkboard-teacher"></i> | Tutores
            </a>
            <div class="navbar-item has-dropdown is-hoverable is-size-6">
            <a class="navbar-link">
              <i class="fas fa-question"></i> | ¿Quiénes somos?
            </a>

            <div class="navbar-dropdown">
              <a href="{{ url('/info-depto') }}" class="navbar-item is-size-6">
                Departamento de tutoría y desarrollo estudiantíl
              </a>
              <hr class="navbar-divider">
              <a href="{{ url('/report-problema') }}" class="navbar-item is-size-6">
                Reportar un problema
              </a>
            </div>
            </div>
            <a href="{{ url('/contact') }}" class="navbar-item is-size-6">
            <i class="fas fa-at"></i> | Contactanos
            </a>

          </div>
          <div class="navbar-end">
            <div class="navbar-item has-dropdown is-hoverable">
            <a class="navbar-link">
              <figure class="image is-32x32" style="margin-right:.5em;">
                <img src="../../images/{{Auth::user()->image}}">
              </figure>
              {{ Auth::user()->name }}
            </a>

            <div class="navbar-dropdown is-right">
                <a class="navbar-item">
                  Perfil
                </a>
                <hr class="navbar-divider">
                <a class="navbar-item" href="{{ route('logout') }}">
                  Salir
                </a>
            </div>
          </div>
          </div>
          @endguest
      </div>
    </nav>


    <section class="hero is-primary is-flex">
      <!-- Hero content: will be in the middle -->
      <div class="hero-body">
        <div class="container has-text-centered">
          <h1 class="title">
              <figure class="image center">
                    <div class="columns">
                            <div class="column center">
                                <img src="http://utcancun.edu.mx/wp-content/uploads/2016/06/Logo-UT-01.png" style="max-width: 215px">
                            </div>
                            <div class="column">
                                <img src="../../images/logo.png" style="max-width: 215px">
                            </div>
                    </div>
              </figure>
          </h1>
          <h2 class="is-size-3">
            Plataforma de Tutoría entre pares
          </h2>
        </div>
      </div>
    </section>

    <!-- Body -->
    @yield('body')
    <hr>
    <div class="container is-flex">

    </div>

    <!-- Footer -->
    <footer class="footer is-flex">
      <div class="content has-text-centered">
       <font size="1" face="Comic Sans MS, Arial, MS Sans Serif">
        <p style="text-align: justify">
          En cumplimiento a Ley General de Protección de Datos Personales en Posesión de los Sujetos Obligados y la Ley de Protección de Datos Personales en Posesión de Sujetos Obligados para el Estado de Quintana Roo, la UT Cancún en su calidad de Sujeto Obligado informa que es el responsable del tratamiento de los Datos Personales que nos proporcione, los cuales serán protegidos de conformidad con lo dispuesto en los citados ordenamientos y demás que resulten aplicables. La información de carácter personal aquí proporcionada, únicamente podrá ser utilizada para el análisis y estadísticos del seguimiento a las actividades de la función tutorial, que permitan atender las situaciones de riesgo y realizar estrategias que garanticen la permanencia de los estudiantes,  asumiendo la obligación de cumplir con las medidas legales y de seguridad suficientes para proteger los Datos Personales que se hayan recabado. Para mayor detalle consulte, nuestro Aviso de Privacidad Integral en: www.utcancun.edu.mx en la sección de “Avisos de Privacidad”.
        </font>
          <br>
          <br>
          <br>
        </p>
        <p>
          <strong>Plataforma de tutoría entre pares</strong> por <a href="http://utcancun.edu.mx/">Universidad Tecnológica de Cancún</a> &copy; &#174; 2019
        </p>
        <div><i class="fab fa-facebook"></i><a href="https://www.facebook.com/UTdeCancun/?ref=br_rs"> Facebook</div></a>
        <div><i class="fab fa-instagram"></i><a href="https://www.instagram.com/utcancun/"> Instagram</a></div>
       <div><i class="fab fa-twitter"></i><a href="https://twitter.com/utcancun?lang=es"> Twitter</a></div>
      </div>
    </footer>

    <script src="https://unpkg.com/bulma-modal-fx/dist/js/modal-fx.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script type="text/javascript" src="https://unpkg.com/bulma-modal-fx/dist/js/modal-fx.min.js"></script>
    <script async type="text/javascript" src="../../js/bulma.js"></script>
    <script src="../../js/wild.js"></script>
    <script src="../../js/app.js"></script>

  </body>
</html>
