<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comentario extends Model
{
    protected $table = 'comentarios';
    protected $primaryKey = 'idComentario';

    protected $fillable = [
        'contenidoComentario', 'anexoComentario', 'hilos_idHilo', 'users_id', 'created_at'
    ];

    public $timestamps = false;
}
