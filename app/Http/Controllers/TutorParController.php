<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class TutorParController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tutorPar = User::select('users.id', 'users.name', 'users.email', 'carreras.nombreCarrera', 'users.grupoAlumno', 'users.cuatrimestreAlumno', 'users.sexoAlumno', 'users.divisionAlumno', 'users.image')
        ->join('carreras', 'users.carreras_idCarrera', '=', 'carreras.idCarrera')
        ->where('rolUser', 'tutorpar')
        ->get();

        return view('back.tutorPar.index', ['tutorPar'=>$tutorPar]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tutorPar = new User;
        $request = request();
        $carrera = 0;
        $cuatrimestre = "";

        $profileImage = $request->file('image');
        if($profileImage == null){
            $profile_image_url = 'NoImage';
        }else{
            $profileImageSaveAsName = time() . "-profile." .
            $profileImage->getClientOriginalExtension();
            $upload_path = 'images/';
            $profile_image_url = $profileImageSaveAsName;
            $success = $profileImage->move($upload_path, $profileImageSaveAsName);
        }

        if($request['carrera'] == "INGENIERÍA EN TECNOLOGÍAS DE LA INFORMACIÓN Y COMUNICACIÓN"){
            $carrera = 1;
        }elseif($request['carrera'] == "TÉCNICO SUPERIOR UNIVERSITARIO EN TECNOLOGÍAS DE LA INFORMACIÓN Y COMUNICACIÓN, ÁREA SISTEMAS INFORMÁTICOS"){
            $carrera = 2;
        }elseif($request['carrera'] == "TÉCNICO SUPERIOR UNIVERSITARIO EN TECNOLOGÍAS DE LA INFORMACIÓN Y COMUNICACIÓN, ÁREA REDES Y TELECOMUNICACIONES"){
            $carrera = 3;
        }elseif($request['carrera'] == "TÉCNICO SUPERIOR UNIVERSITARIO EN MANTENIMIENTO, ÁREA INSTALACIONES"){
            $carrera = 4;
        }elseif($request['carrera'] == "TÉCNICO SUPERIOR UNIVERSITARIO EN GASTRONOMÍA"){
            $carrera = 5;
        }elseif($request['carrera'] == "TÉCNICO SUPERIOR UNIVERSITARIO EN ADMINISTRACIÓN, ÁREA RECURSOS HUMANOS"){
            $carrera = 6;
        }elseif($request['carrera'] == "TÉCNICO SUPERIOR UNIVERSITARIO EN CONTADURÍA"){
            $carrera = 7;
        }elseif($request['carrera'] == "TÉCNICO SUPERIOR UNIVERSITARIO EN DESARROLLO DE NEGOCIOS, ÁREA MERCADOTECNIA"){
            $carrera = 8;
        }elseif($request['carrera'] == "TÉCNICO SUPERIOR UNIVERSITARIO EN TURISMO, ÁREA HOTELERIA"){
            $carrera = 9;
        }elseif($request['carrera'] == "TÉCNICO SUPERIOR UNIVERSITARIO EN TURISMO, ÁREA DESARROLLO DE PRODUCTOS ALTERNATIVOS"){
            $carrera = 10;
        }elseif($request['carrera'] == "TÉCNICO SUPERIOR UNIVERSITARIO EN TERAPIA FÍSICA, ÁREA TURISMO DE SALUD Y BIENESTAR"){
            $carrera = 11;
        }elseif($request['carrera'] == "INGENIERÍA EN DESARROLLO E INNOVACIÓN EMPRESARIAL"){
            $carrera = 12;
        }elseif($request['carrera'] == "INGENIERÍA EN MANTENIMIENTO INDUSTRIAL"){
            $carrera = 13;
        }elseif($request['carrera'] == "LICENCIATURA EN GESTIÓN Y DESARROLLO TURÍSTICO"){
            $carrera = 14;
        }elseif($request['carrera'] == "INGENIERÍA FINANCIERA Y FISCAL"){
            $carrera = 15;
        }elseif($request['carrera'] == "LICENCIATURA EN GASTRONOMÍA"){
            $carrera = 16;
        }elseif($request['carrera'] == "TÉCNICO SUPERIOR UNIVERSITARIO EN ADMINISTRACIÓN, ÁREA CAPITAL HUMANO"){
            $carrera = 17;
        }elseif($request['carrera'] == "TÉCNICO SUPERIOR UNIVERSITARIO EN TECNOLOGÍAS DE LA INFORMACIÓN, ÁREA DESARROLLO DE SOFTWARE MULTIPLATAFORMA"){
            $carrera = 18;
        }elseif($request['carrera'] == "TÉCNICO SUPERIOR UNIVERSITARIO EN TECNOLOGÍAS DE LA INFORMACIÓN, ÁREA INFRAESTRUCTURA DE REDES DIGITALES"){
            $carrera = 19;
        }elseif($request['carrera'] == "TÉCNICO SUPERIOR UNIVERSITARIO EN MANTENIMIENTO, ÁREA INSTALACIONES"){
            $carrera = 20;
        }

        $tutorPar->name = $request->name;
        $tutorPar->email = $request->correo;
        $tutorPar->password = Hash::make($request->contra);
        $tutorPar->nombreAlumno = $request->name;
        $tutorPar->matriculaAlumno = $request->matricula;
        $tutorPar->carreras_idCarrera = $carrera;
        $tutorPar->curpAlumno = $request->curp;
        $tutorPar->grupoAlumno = $request->grupo;
        $tutorPar->cuatrimestreAlumno = $request->cuatrimestre;
        $tutorPar->fechaNacAlumno = $request->fechaNacimiento;
        $tutorPar->sexoAlumno = $request->sexo;
        $tutorPar->divisionAlumno = $request->division;
        $tutorPar->rolUser = 'tutorpar';
        $tutorPar->image = $profile_image_url;
        $tutorPar->soyAlumno = $request->soyAlumno;
        $tutorPar->conAlumno1 = $request->conAlumno1;
        $tutorPar->conAlumno2 = $request->conAlumno2;
        $tutorPar->conAlumno3 = $request->conAlumno3;
        $tutorPar->habAlumno1 = $request->habAlumno1;
        $tutorPar->habAlumno2 = $request->habAlumno2;
        $tutorPar->habAlumno3 = $request->habAlumno3;
        $tutorPar->habAlumno4 = $request->habAlumno4;
        $tutorPar->actAlumno1 = $request->actAlumno1;
        $tutorPar->actAlumno2 = $request->actAlumno2;
        $tutorPar->actAlumno3 = $request->actAlumno3;
        $tutorPar->actAlumno4 = $request->actAlumno4;
        
        $tutorPar->save();

        return redirect()->route('tutoresPar.index')->with('status', 'Se ha dado de alta correctamente al TutorPar :).');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }
}
