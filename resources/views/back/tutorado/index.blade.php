@extends('layouts.back')

@section('body')


<hr>
<div class="container">



    <div class="columns">

                <div class="column is-3 ">
                    <aside class="menu is-hidden-mobile">
                        <p class="menu-label">
                            General
                        </p>
                        <ul class="menu-list">
                            <li><a class="menu-label" href="/home">Dashboard</a></li>
                        </ul>
                        <p class="menu-label">
                            Administración
                        </p>
                        <ul class="menu-list">
                            <li>
                                <a>Administración de usuarios</a>
                                <ul>
                                    <li><a class="is-active" href="/tutorados">Tutorados</a></li>
                                    <li><a href="/tutoresPar">Tutores Par</a></li>
                                    <li><a>Administradores</a></li>
                                    <li><a>Carreras</a></li>
                                </ul>
                            </li>
                            <li><a>Citas</a></li>
                            <li><a>Foros</a></li>
                        </ul>
                        <p class="menu-label">
                            Otros
                        </p>
                        <ul class="menu-list">
                            <li><a>Reporte</a></li>
                            <li><a>Reporte</a></li>
                            <li><a>Reporte</a></li>
                        </ul>
                    </aside>
                </div>

                <div class="column is-9">
                  <div class="box cta">
                    <nav class="breadcrumb" aria-label="breadcrumbs">
                      <ul>
                        <li>
                          <a href="#">
                            <span class="icon is-small">
                              <i class="fas fa-users-cog"></i>
                            </span>
                            <span>Administración</span>
                          </a>
                        </li>
                        <li>
                          <a href="#">
                            <span class="icon is-small">
                              <i class="fas fa-user"></i>
                            </span>
                            <span>Gestión de tutorados</span>
                          </a>
                        </li>
                      </ul>
                    </nav>
                  </div>

                    <section class="info-tiles">
                        <div class="tile is-ancestor has-text-centered">
                            <div class="tile is-parent">
                                <article class="tile is-child box">
                                  <a class="button is-link is-focused is-fullwidth">Registrar Nuevo Tutorado</a>
                                </article>
                            </div>
                            <div class="tile is-parent">
                                <article class="tile is-child box">
                                  <a class="button is-primary is-focused is-fullwidth">Importar Archivo CSV</a>
                                </article>
                            </div>
                        </div>
                    </section>

                    <hr>
                    <div class="box cta">
                    <table class="table">
                      <thead>
                        <tr>
                          <th><abbr title="ID">ID</abbr></th>
                          <th>Nombre</th>
                          <th><abbr title="Correo">Correo</abbr></th>
                          <th><abbr title="Carrera">Carrera</abbr></th>
                          <th><abbr title="Grupo">Grupo</abbr></th>
                          <th><abbr title="Cuatrimestre">Cuatrimestre</abbr></th>
                          <th><abbr title="Sexo">Sexo</abbr></th>
                          <th><abbr title="Foto">Foto</abbr></th>
                        </tr>
                      </thead>
                      <tbody>
                      @foreach($tutorado as $tutorados)
                        <tr>
                          
                          <th>{{ $tutorados->id }}</th>
                          <td>{{ $tutorados->name }}</td>
                          <td>{{ $tutorados->email }}</td>
                          <td>{{ $tutorados->nombreCarrera }}</td>
                          <td>{{ $tutorados->grupoAlumno }}</td>
                          <td>{{ $tutorados->cuatrimestreAlumno }}</td>
                          <td>{{ $tutorados->sexoAlumno }}</td>
                          <td>
                            <figure class="image is-128x128">
                              <img src="../../images/{{ $tutorados->image }}">
                            </figure>
                          </td>
                         
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                    </div>

                </div>
            </div>


</div>


@endsection
