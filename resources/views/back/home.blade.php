@extends('layouts.back')

@section('body')

<hr>
<div class="container">


  <div class="columns">

              <div class="column is-3 ">
                  <aside class="menu is-hidden-mobile">
                      <p class="menu-label">
                          General
                      </p>
                      <ul class="menu-list">
                          <li><a class="is-active" href="/home">Dashboard</a></li>
                      </ul>
                      <p class="menu-label">
                          Administración
                      </p>
                      <ul class="menu-list">
                          <li>
                              <a>Administración de usuarios</a>
                              <ul>
                                  <li><a href="/tutorados">Tutorados</a></li>
                                  <li><a>Tutores Par</a></li>
                                  <li><a>Administradores</a></li>
                                  <li><a>Carreras</a></li>
                              </ul>
                          </li>
                          <li><a>Citas</a></li>
                          <li><a>Foros</a></li>
                      </ul>
                      <p class="menu-label">
                          Otros
                      </p>
                      <ul class="menu-list">
                          <li><a>Reporte</a></li>
                          <li><a>Reporte</a></li>
                          <li><a>Reporte</a></li>
                      </ul>
                  </aside>
              </div>

              <div class="column is-9">
                <div class="box cta">
                  <nav class="breadcrumb" aria-label="breadcrumbs">
                    <ul>
                      <li>
                        <a href="#">
                          <span class="icon is-small">
                            <i class="fas fa-users-cog"></i>
                          </span>
                          <span>Administración</span>
                        </a>
                      </li>
                    </ul>
                  </nav>
                </div>
                  <section class="hero is-info welcome is-small">
                      <div class="hero-body">
                          <div class="container">
                              <h1 class="title">
                                  Bienvenido, {{ Auth::user()->name }}
                              </h1>
                              <h2 class="subtitle">
                                  ¡Espero estés teniendo un excelente día!
                              </h2>
                          </div>
                      </div>
                  </section>
                  <hr>
                  <section class="info-tiles">
                      <div class="tile is-ancestor has-text-centered">
                          <div class="tile is-parent">
                              <article class="tile is-child box">
                                  <p class="title">439k</p>
                                  <p class="subtitle">Users</p>
                              </article>
                          </div>
                          <div class="tile is-parent">
                              <article class="tile is-child box">
                                  <p class="title">59k</p>
                                  <p class="subtitle">Products</p>
                              </article>
                          </div>
                          <div class="tile is-parent">
                              <article class="tile is-child box">
                                  <p class="title">3.4k</p>
                                  <p class="subtitle">Open Orders</p>
                              </article>
                          </div>
                          <div class="tile is-parent">
                              <article class="tile is-child box">
                                  <p class="title">19</p>
                                  <p class="subtitle">Exceptions</p>
                              </article>
                          </div>
                      </div>
                  </section>

              </div>
          </div>






</div>

@endsection
