<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class TutoradoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tutorado = User::select('users.id', 'users.name', 'users.email', 'carreras.nombreCarrera', 'users.grupoAlumno', 'users.cuatrimestreAlumno', 'users.sexoAlumno', 'users.divisionAlumno', 'users.image')
        ->join('carreras', 'users.carreras_idCarrera', '=', 'carreras.idCarrera')
        ->where('rolUser', 'tutorado')
        ->get();

        return view('back.tutorado.index', ['tutorado'=>$tutorado]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
