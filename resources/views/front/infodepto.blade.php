@extends('layouts.front')

@section('body')

<hr>
<div class="container">

  <div class="box cta">
    <nav class="breadcrumb" aria-label="breadcrumbs">
      <ul>
        <li>
          <a href="/">
            <span class="icon is-small">
              <i class="fas fa-home" aria-hidden="true"></i>
            </span>
            <span>Bienvenidos</span>
          </a>
        </li>
        <li>
          <a href="/info-depto">
            <span class="icon is-small">
              <i class="fas fa-home" aria-hidden="true"></i>
            </span>
            <span>Departamento de tutoría y desarrollo estudiantíl</span>
          </a>
        </li>
      </ul>
    </nav>
    <hr>
    <p class="has-text-centered">
      <span class="tag is-primary"><i class="fas fa-hand-paper"></i></span> Bienvenido a la plataforma de tutoria entre pares.
    </p>
  </div>
  <!--editar asjdgajsdkas-->

  <div class="box">
    <h1 class="title"><p style="text-align: center;" >Información sobre el Departamento</p></h1>
    <div class="card">
  <!-- <div class="card-image">
    <figure class="image is-4by3">
      <img src="https://bulma.io/images/placeholders/1280x960.png" alt="Placeholder image">
    </figure>
  </div> -->
  <div class="card-content">
    <div class="media">
      <div class="media-left">
        <figure class="image is-48x48">
          <img src="http://utcancun.edu.mx/wp-content/uploads/2018/10/BIS-Vertical-02.png" alt="Placeholder image">
        </figure>
      </div>
      <div class="media-content">
        <p class="title is-4">Departamento tutoría y Desarrollo estudiantil</p>
        <!-- <p class="subtitle is-6">@johnsmith</p> -->
      </div>
    </div>

    <div class="content">
      El departamento de tutoría y Desarrollo estudiantil se dio a la tarea de realizar un sistema capaz de brindar tutoría a alumnos de nuevo ingreso con el fin de poder ayudar y mejorar el desempeño de estos mismos, dicho sistema se llama "Tutoría entre pares" que lo que busca es poder ofrecer a los alumnos de nuevo ingreso la confianza de expresar, resolver dudas y problemas que surgan en el ámbito escolar dentro de la Universidad Tecnológica de Cancún <!-- <a>@bulmaio</a>.
      <a href="#">#css</a> <a href="#">#responsive</a> -->
      <br>
      <!-- <time datetime="2016-1-1">11:09 PM - 1 Jan 2016</time> -->
    </div>
    <div class="content" align="center">
     <strong>¿Dónde se ubica el DT y DE?</strong><br><br>
     Te esperamos en el Edificio G, planta baja.<br>
     Horarios de atención: De 8:00 a 15:00 hrs.
   </div>
   <div class="content" align="center">
    <strong>Contacto</strong><br><br>
    Lic. Nayely Mancilla Santos<br>

    Jefa de Departamento tutoría<br>
    Teléfono directo: (8811900)<br>

    Ext. 1315
  </div>
</div>
</div>


</div>




<!--fin edición-->
</div>

@endsection