<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        return $this->registered($request, $user)
            ?: redirect($this->redirectPath());
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $request = request();
        $carrera = 0;
        $cuatrimestre = "";

        $profileImage = $request->file('image');
        if($profileImage == null){
            $profile_image_url = 'user.png';
        }else{
            $profileImageSaveAsName = time() . $data['matricula'] ."-profile." .
            $profileImage->getClientOriginalExtension();
            $upload_path = 'images/';
            $profile_image_url = $profileImageSaveAsName;
            $success = $profileImage->move($upload_path, $profileImageSaveAsName);
        }


        if($data['carrera'] == "INGENIERÍA EN TECNOLOGÍAS DE LA INFORMACIÓN Y COMUNICACIÓN"){
            $carrera = 1;
        }elseif($data['carrera'] == "TÉCNICO SUPERIOR UNIVERSITARIO EN TECNOLOGÍAS DE LA INFORMACIÓN Y COMUNICACIÓN, ÁREA SISTEMAS INFORMÁTICOS"){
            $carrera = 2;
        }elseif($data['carrera'] == "TÉCNICO SUPERIOR UNIVERSITARIO EN TECNOLOGÍAS DE LA INFORMACIÓN Y COMUNICACIÓN, ÁREA REDES Y TELECOMUNICACIONES"){
            $carrera = 3;
        }elseif($data['carrera'] == "TÉCNICO SUPERIOR UNIVERSITARIO EN MANTENIMIENTO, ÁREA INSTALACIONES"){
            $carrera = 4;
        }elseif($data['carrera'] == "TÉCNICO SUPERIOR UNIVERSITARIO EN GASTRONOMÍA"){
            $carrera = 5;
        }elseif($data['carrera'] == "TÉCNICO SUPERIOR UNIVERSITARIO EN ADMINISTRACIÓN, ÁREA RECURSOS HUMANOS"){
            $carrera = 6;
        }elseif($data['carrera'] == "TÉCNICO SUPERIOR UNIVERSITARIO EN CONTADURÍA"){
            $carrera = 7;
        }elseif($data['carrera'] == "TÉCNICO SUPERIOR UNIVERSITARIO EN DESARROLLO DE NEGOCIOS, ÁREA MERCADOTECNIA"){
            $carrera = 8;
        }elseif($data['carrera'] == "TÉCNICO SUPERIOR UNIVERSITARIO EN TURISMO, ÁREA HOTELERIA"){
            $carrera = 9;
        }elseif($data['carrera'] == "TÉCNICO SUPERIOR UNIVERSITARIO EN TURISMO, ÁREA DESARROLLO DE PRODUCTOS ALTERNATIVOS"){
            $carrera = 10;
        }elseif($data['carrera'] == "TÉCNICO SUPERIOR UNIVERSITARIO EN TERAPIA FÍSICA, ÁREA TURISMO DE SALUD Y BIENESTAR"){
            $carrera = 11;
        }elseif($data['carrera'] == "INGENIERÍA EN DESARROLLO E INNOVACIÓN EMPRESARIAL"){
            $carrera = 12;
        }elseif($data['carrera'] == "INGENIERÍA EN MANTENIMIENTO INDUSTRIAL"){
            $carrera = 13;
        }elseif($data['carrera'] == "LICENCIATURA EN GESTIÓN Y DESARROLLO TURÍSTICO"){
            $carrera = 14;
        }elseif($data['carrera'] == "INGENIERÍA FINANCIERA Y FISCAL"){
            $carrera = 15;
        }elseif($data['carrera'] == "LICENCIATURA EN GASTRONOMÍA"){
            $carrera = 16;
        }elseif($data['carrera'] == "TÉCNICO SUPERIOR UNIVERSITARIO EN ADMINISTRACIÓN, ÁREA CAPITAL HUMANO"){
            $carrera = 17;
        }elseif($data['carrera'] == "TÉCNICO SUPERIOR UNIVERSITARIO EN TECNOLOGÍAS DE LA INFORMACIÓN, ÁREA DESARROLLO DE SOFTWARE MULTIPLATAFORMA"){
            $carrera = 18;
        }elseif($data['carrera'] == "TÉCNICO SUPERIOR UNIVERSITARIO EN TECNOLOGÍAS DE LA INFORMACIÓN, ÁREA INFRAESTRUCTURA DE REDES DIGITALES"){
            $carrera = 19;
        }elseif($data['carrera'] == "TÉCNICO SUPERIOR UNIVERSITARIO EN MANTENIMIENTO, ÁREA INSTALACIONES"){
            $carrera = 20;
        }

        if($data['cuatri'] == 'PRIMERO' || $data['cuatri'] == 'SEGUNDO' || $data['cuatri'] == 'TERCER' || $data['cuatri'] == 'CUARTO' || $data['cuatri'] == 'QUINTO' || $data['cuatri'] == 'SEXTO'){
            return User::create([
                'name' => $data['name'],
                'email' => $data['email'],
                'password' => Hash::make($data['password']),
                'nombreAlumno' => $data['name'],
                'matriculaAlumno' => $data['matricula'],
                'carreras_idCarrera' => $carrera,
                'curpAlumno' => $data['curp'],
                'grupoAlumno' => $data['grupo'],
                'cuatrimestreAlumno' => $data['cuatri'],
                'fechaNacAlumno' => $data['fechaNac'],
                'sexoAlumno' => $data['sexo'],
                'divisionAlumno' => $data['division'],
                'rolUser' => 'tutorado',
                'image' => $profile_image_url,
            ]);

            return redirect($this->redirectTo)->with('success', '¡Registro éxitoso!, ahora inicia sesión.');
        }else{
            return redirect($this->redirectTo)->with('error', '¡Error!, Sólo se admiten alumnos de T.S.U. (Por ahora)');
        }



    }
}
