@extends('layouts.front')

@section('body')

    <hr>
    <div class="container">

        @if(session('status'))
        <article class="message is-success">
            <div class="message-header">
            <p>Mensaje</p>
            <button class="delete" aria-label="delete"></button>
            </div>
            <div class="message-body">
            {{ session('status') }}
            </div>
        </article>
        @endif

        <div class="box cta">
        <nav class="breadcrumb" aria-label="breadcrumbs">
            <ul>
            <li>
                <a href="#">
                <span class="icon is-small">
                    <i class="fas fa-home" aria-hidden="true"></i>
                </span>
                <span>Bienvenidos</span>
                </a>
            </li>
            <li>
                <a href="#">
                <span class="icon is-small">
                    <i class="fas fa-align-justify" aria-hidden="true"></i>
                </span>
                <span>Foro</span>
                </a>
            </li>
            </ul>
        </nav>
        <hr>
        <p class="has-text-centered">
            <span class="tag is-primary"><i class="fas fa-hand-paper"></i></span> Estás en la sección "Foro", aquí podrás hacer preguntas y ayudar a tus compañeros en sus dudas.
        </p>
        </div>

        <!-- Comienzo del modal Matricula -->
            <div id="modal-id" class="modal modal-fx-fadeInScale modal-pos-bottom">
                <div class="modal-background"></div>
                <div class="modal-card">

                <div class="card">
                    <header class="card-header">
                    <p class="card-header-title">
                        Nuevo Tema
                    </p>
                    <a class="card-header-icon" aria-label="more options">
                        <span class="icon">
                        <i class="fas fa-angle-down" aria-hidden="true"></i>
                        </span>
                    </a>
                    </header>
                    <div class="card-content">
                        <div class="content">

                            {!! Form::open(['route' => 'foros.store', 'method' => 'POST', 'files' => true]) !!}
                                <div class="field is-horizontal">
                                    <div class="field-label">
                                        {!! Form::label('name', 'Tema: ', ['class' => 'label']) !!}
                                    </div>

                                    <div class="field-body">
                                        <div class="field">
                                            <p class="control">
                                                {!! Form::text('temaHilo', null, ['class' => 'input', 'placeholder' => '¿Cómo realizar un ensayo?', 'required' => 'required']) !!}
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="field is-horizontal">
                                    <div class="field-label">
                                        {!! Form::label('name', 'Contenido: ', ['class' => 'label']) !!}
                                    </div>

                                    <div class="field-body">
                                        <div class="field">
                                            <p class="control">
                                                {!! Form::textarea('contenidoHilo', null, ['class' => 'input', 'placeholder' => 'Expresa tu duda...', 'required' => 'required']) !!}
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="field is-horizontal">
                                    <div class="field-label">
                                        {!! Form::label('name', 'Anexos: ', ['class' => 'label']) !!}
                                    </div>

                                    {!! Form::file('image', null, ['class' => 'file-input']) !!}

                                    {!! Form::hidden('users_id', Auth::user()->id, ['class' => 'hidden']) !!}

                                </div>
                                <hr>
                                {!! Form::submit('Publicar', ['class' => 'button is-block is-large is-primary', 'style' => 'float:right;']) !!}

                            {!! Form::close() !!}

                        </div>
                    </div>

                </div>

                </div>
            </div>
         <!-- Final del modal Matricula -->

        <section class="container">
			<div class="columns">
				<div class="column is-3">
                    <span class="button is-primary is-block is-alt is-large modal-button" data-target="modal-id">Nuevo Tema</span>
				</div>
				<div class="column is-9">
					<div class="box content">
                        @foreach($foro as $foros)
						<article class="post">
							<h4> <a href="/foros/{{$foros->idHilo}}"> {{$foros->temaHilo}} </a> </h4>
							<div class="media">
								<div class="media-left">
									<p class="image is-32x32">
										<img src="../../images/{{ $foros->image }}">
									</p>
								</div>
								<div class="media-content is-flex">
									<div class="content">
										<p>
											<b>{{$foros->name}}</b> | {{$foros->contenidoHilo}}
											<span class="tag">Pregunta</span>
										</p>
									</div>
								</div>
								<div class="media-right">
									<span class="has-text-grey-light"><i class="fa fa-comments"></i> 1</span>
								</div>
							</div>
						</article>
                        <hr>
						@endforeach
					</div>
				</div>
			</div>
		</section>

    </div>

@endsection
