<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Foro extends Model
{
    protected $table = 'hilos';
    protected $primaryKey = 'idHilo';

    protected $fillable = [
        'temaHilo', 'contenidoHilo', 'anexoHilo', 'users_id', 'created_at' 
    ];

    public $timestamps = false;
}
