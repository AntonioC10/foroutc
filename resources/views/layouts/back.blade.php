<!DOCTYPE html>
<html class="has-navbar-fixed-top">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Tutoría en pares</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.4/css/bulma.min.css">
    <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
  </head>
  <body>

    <!-- NavBar -->
    <nav class="navbar is-fixed-top is-primary" role="navigation" aria-label="main navigation">
    <div class="navbar-brand">
    <a class="navbar-item" href="{{ url('Bienvenidos') }}">
    <img src="https://bulma.io/images/bulma-logo.png" width="112" height="28">
    </a>

    <a role="button" class="navbar-burger burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
    <span aria-hidden="true"></span>
    <span aria-hidden="true"></span>
    <span aria-hidden="true"></span>
    </a>
    </div>

    <div id="navbarBasicExample" class="navbar-menu">
      <div class="navbar-end">

          <div class="navbar-item has-dropdown is-hoverable">
          <a class="navbar-link">
            <figure class="image is-32x32" style="margin-right:.5em;">
              <img src="">
            </figure>
            {{ Auth::user()->name }}
          </a>

          <div class="navbar-dropdown is-right">
              <a class="navbar-item">
                Perfil
              </a>
              <hr class="navbar-divider">
              <a class="navbar-item" href="{{ route('logout') }}">
                Salir
              </a>
          </div>
        </div>
        
      </div>
    </nav>

    <!-- Body -->
    @yield('body')

    <hr>
    <!-- Footer -->
    <footer class="footer">
      <div class="content has-text-centered">
        <p>
          <strong>Plataforma de tutoría en pares</strong> por <a href="http://utcancun.edu.mx/">Universidad Tecnológica de Cancún</a> (R) 2019
        </p>
      </div>
    </footer>

    <script src="https://unpkg.com/bulma-modal-fx/dist/js/modal-fx.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script async type="text/javascript" src="../js/bulma.js"></script>
    <script src="../js/wild.js"></script>

  </body>
</html>
