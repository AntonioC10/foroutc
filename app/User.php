<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'nombreAlumno', 'matriculaAlumno',
        'carreras_idCarrera', 'curpAlumno', 'grupoAlumno', 'grupoAlumno', 'cuatrimestreAlumno',
        'fechaNacAlumno', 'sexoAlumno', 'divisionAlumno', 'rolUser', 'image', 
        'soyAlumno', 'conAlumno1', 'colAlumno2', 'conAlumno3', 'habAlumno1', 
        'habAlumno2', 'habAlumno3', 'habAlumno4', 'actAlumno1', 'actAlumno2', 
        'actAlumno3', 'actAlumno4'
      ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
