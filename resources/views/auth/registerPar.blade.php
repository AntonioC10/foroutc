@extends('layouts.front')

@section('body')

<hr>
    <div class="columns is-marginless is-centered">
        <div class="column is-5">
            <div class="card">
                <header class="card-header">
                    <p class="card-header-title">Registrar nuevo tutor par</p>
                </header>
                <div class="card-content">
                {!! Form::open(['route' => 'tutoresPar.store', 'method' => 'POST', 'files' => true]) !!}

                        <div class="field is-horizontal">
                            <div class="field-label">
                                {!! Form::label('name', 'Nombre Completo: ', ['class' => 'label']) !!}
                            </div>
                            <div class="field-body">
                                <div class="field">
                                    <p class="control">
                                        {!! Form::text('name', $data->nombreCompleto, ['class' => 'input', 'readonly']) !!}
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="field is-horizontal">
                            <div class="field-label">
                                {!! Form::label('matricula', 'Matricula: ', ['class' => 'label']) !!}
                            </div>
                            <div class="field-body">
                                <div class="field">
                                    <p class="control">
                                        {!! Form::text('matricula', $data->matricula, ['class' => 'input', 'readonly']) !!}
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="field is-horizontal">
                            <div class="field-label">
                                {!! Form::label('carrera', 'Carrera: ', ['class' => 'label']) !!}
                            </div>
                            <div class="field-body">
                                <div class="field">
                                    <p class="control">
                                        {!! Form::text('carrera', $data->carrera, ['class' => 'input', 'readonly']) !!}
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="field is-horizontal">
                            <div class="field-label">
                                {!! Form::label('carrera', 'CURP: ', ['class' => 'label']) !!}
                            </div>
                            <div class="field-body">
                                <div class="field">
                                    <p class="control">
                                        {!! Form::text('curp', $data->curp, ['class' => 'input', 'readonly']) !!}
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="field is-horizontal">
                            <div class="field-label">
                                {!! Form::label('carrera', 'Grupo: ', ['class' => 'label']) !!}
                            </div>
                            <div class="field-body">
                                <div class="field">
                                    <p class="control">
                                        {!! Form::text('grupo', $data->grupo, ['class' => 'input', 'readonly']) !!}
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="field is-horizontal">
                            <div class="field-label">
                                {!! Form::label('carrera', 'Cuatrimestre: ', ['class' => 'label']) !!}
                            </div>
                            <div class="field-body">
                                <div class="field">
                                    <p class="control">
                                        {!! Form::text('cuatrimestre', $data->cuatrimestre, ['class' => 'input', 'readonly']) !!}
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="field is-horizontal">
                            <div class="field-label">
                                {!! Form::label('carrera', 'Fecha de nacimiento: ', ['class' => 'label']) !!}
                            </div>
                            <div class="field-body">
                                <div class="field">
                                    <p class="control">
                                        {!! Form::text('fechaNacimiento', $data->fechaNacimiento, ['class' => 'input', 'readonly']) !!}
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="field is-horizontal">
                            <div class="field-label">
                                {!! Form::label('carrera', 'Sexo: ', ['class' => 'label']) !!}
                            </div>
                            <div class="field-body">
                                <div class="field">
                                    <p class="control">
                                        {!! Form::text('sexo', $data->sexo, ['class' => 'input', 'readonly']) !!}
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="field is-horizontal">
                            <div class="field-label">
                                {!! Form::label('carrera', 'División: ', ['class' => 'label']) !!}
                            </div>
                            <div class="field-body">
                                <div class="field">
                                    <p class="control">
                                        {!! Form::text('division', $data->division, ['class' => 'input', 'readonly']) !!}
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="field is-horizontal">
                            <div class="field-label">
                                {!! Form::label('carrera', '¿Quién soy?', ['class' => 'label']) !!}
                            </div>
                            <div class="field-body">
                                <div class="field">
                                    <p class="control">
                                        {!! Form::textarea('soyAlumno', null, ['class' => 'input']) !!}
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="field is-horizontal">
                            <div class="field-label">
                                {!! Form::label('carrera', 'Conocimiento #1: ', ['class' => 'label']) !!}
                            </div>
                            <div class="field-body">
                                <div class="field">
                                    <p class="control">
                                        {!! Form::textarea('conAlumno1', null, ['class' => 'input']) !!}
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="field is-horizontal">
                            <div class="field-label">
                                {!! Form::label('carrera', 'Conocimiento #2: ', ['class' => 'label']) !!}
                            </div>
                            <div class="field-body">
                                <div class="field">
                                    <p class="control">
                                        {!! Form::textarea('conAlumno2', null, ['class' => 'input']) !!}
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="field is-horizontal">
                            <div class="field-label">
                                {!! Form::label('carrera', 'Conocimiento #3: ', ['class' => 'label']) !!}
                            </div>
                            <div class="field-body">
                                <div class="field">
                                    <p class="control">
                                        {!! Form::textarea('conAlumno3', null, ['class' => 'input']) !!}
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="field is-horizontal">
                            <div class="field-label">
                                {!! Form::label('carrera', 'Habilidad #1: ', ['class' => 'label']) !!}
                            </div>
                            <div class="field-body">
                                <div class="field">
                                    <p class="control">
                                        {!! Form::textarea('habAlumno1', null, ['class' => 'input']) !!}
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="field is-horizontal">
                            <div class="field-label">
                                {!! Form::label('carrera', 'Habilidad #2: ', ['class' => 'label']) !!}
                            </div>
                            <div class="field-body">
                                <div class="field">
                                    <p class="control">
                                        {!! Form::textarea('habAlumno2', null, ['class' => 'input']) !!}
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="field is-horizontal">
                            <div class="field-label">
                                {!! Form::label('carrera', 'Habilidad #3: ', ['class' => 'label']) !!}
                            </div>
                            <div class="field-body">
                                <div class="field">
                                    <p class="control">
                                        {!! Form::textarea('habAlumno3', null, ['class' => 'input']) !!}
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="field is-horizontal">
                            <div class="field-label">
                                {!! Form::label('carrera', 'Habilidad #4: ', ['class' => 'label']) !!}
                            </div>
                            <div class="field-body">
                                <div class="field">
                                    <p class="control">
                                        {!! Form::textarea('habAlumno4', null, ['class' => 'input']) !!}
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="field is-horizontal">
                            <div class="field-label">
                                {!! Form::label('carrera', 'Actitud #1: ', ['class' => 'label']) !!}
                            </div>
                            <div class="field-body">
                                <div class="field">
                                    <p class="control">
                                        {!! Form::textarea('actAlumno1', null, ['class' => 'input']) !!}
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="field is-horizontal">
                            <div class="field-label">
                                {!! Form::label('carrera', 'Actitud #2: ', ['class' => 'label']) !!}
                            </div>
                            <div class="field-body">
                                <div class="field">
                                    <p class="control">
                                        {!! Form::textarea('actAlumno2', null, ['class' => 'input']) !!}
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="field is-horizontal">
                            <div class="field-label">
                                {!! Form::label('carrera', 'Actitud #3: ', ['class' => 'label']) !!}
                            </div>
                            <div class="field-body">
                                <div class="field">
                                    <p class="control">
                                        {!! Form::textarea('actAlumno3', null, ['class' => 'input']) !!}
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="field is-horizontal">
                            <div class="field-label">
                                {!! Form::label('carrera', 'Correo Electrónico: ', ['class' => 'label']) !!}
                            </div>
                            <div class="field-body">
                                <div class="field">
                                    <p class="control">
                                        {!! Form::text('correo', null, ['class' => 'input']) !!}
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="field is-horizontal">
                            <div class="field-label">
                                {!! Form::label('carrera', 'Contraseña: ', ['class' => 'label']) !!}
                            </div>
                            <div class="field-body">
                                <div class="field">
                                    <p class="control">
                                        {!! Form::password('contra', null, ['class' => 'input', 'type' => 'password']) !!}
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="field is-horizontal">
                            <div class="field-label">
                                {!! Form::label('carrera', 'Confirmar contraseña: ', ['class' => 'label']) !!}
                            </div>
                            <div class="field-body">
                                <div class="field">
                                    <p class="control">
                                        {!! Form::password('contra', null, ['class' => 'input']) !!}
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="field is-horizontal">
                            <div class="field-label">
                                {!! Form::label('carrera', 'Fotografía: ', ['class' => 'label']) !!}
                            </div>
                            <div class="field-body">
                                <div class="field">
                                    <p class="control">
                                        {!! Form::file('image', null, ['class' => 'file-input']) !!}
                                    </p>
                                </div>
                            </div>
                        </div>

                        

                    {!! Form::submit('Registrar', ['class' => 'button is-block is-large is-primary', 'style' => 'float:right;']) !!}

                {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection