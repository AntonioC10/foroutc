<?php

namespace App\Http\Controllers;

use App\Comentario;
use App\Foro;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Collection;

class ForoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $foro = Foro::select('idHilo', 'temaHilo', 'contenidoHilo', 'name', 'image')
        ->join('users', 'hilos.users_id', '=', 'users.id')
        ->get();
        
        return view('front.foro.index', ['foro'=>$foro]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $hilo = new Foro;

        if($request->hasFile('image')){
          $file = $request->file('image');
          $foto = time().$file->getClientOriginalName();
          $file->move(public_path().'/images/', $foto);
        }

        $hilo->temaHilo = $request->temaHilo;
        $hilo->contenidoHilo = $request->contenidoHilo;
        $hilo->anexoHilo = $foto;
        $hilo->users_id = $request->users_id;
        $hilo->created_at = date('Y-m-d H:i:s');
        $hilo->save();

        return redirect()->route('foros.index')->with('status', 'Se ha publicado correctamente tu Tema :).');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Foro  $foro
     * @return \Illuminate\Http\Response
     */
    public function show($idHilo)
    {
        $foro = Foro::find($idHilo);

        $foroDetalle = \DB::table('hilos')
        ->select('hilos.idHilo', 'hilos.temaHilo', 'hilos.contenidoHilo', 'hilos.anexoHilo', 'users.name', 'users.image')
        ->join('users', 'hilos.users_id', '=', 'users.id')
        ->where('hilos.idHilo', $idHilo)
        ->first();

        $comentario = \DB::table('comentarios')
        ->select('comentarios.idComentario', 'comentarios.contenidoComentario', 'comentarios.anexoComentario', 'comentarios.hilos_idHilo', 'users.name', 'users.image')
        ->join('hilos', 'comentarios.hilos_idHilo', '=', 'hilos.idHilo')
        ->join('users', 'comentarios.users_id', '=', 'users.id')
        ->where('hilos_idHilo', $idHilo)
        ->get();

        return view('front.foro.show', compact('foro', 'foroDetalle', 'comentario'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Foro  $foro
     * @return \Illuminate\Http\Response
     */
    public function edit(Foro $foro)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Foro  $foro
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Foro $foro)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Foro  $foro
     * @return \Illuminate\Http\Response
     */
    public function destroy(Foro $foro)
    {
        //
    }
}
