<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cita extends Model
{
    protected $table = 'citas';
    protected $primaryKey = 'idCita';

    protected $fillable = [
        'motivoCita', 'asignaturaCita', 'fechaCita', 'users_id'
    ];

    public $timestamps = false;
}
