@extends('layouts.front')

@section('body')

<hr>
<div class="container">

  <div class="box cta">
    <nav class="breadcrumb" aria-label="breadcrumbs">
      <ul>
        <li>
          <a href="#">
            <span class="icon is-small">
              <i class="fas fa-home" aria-hidden="true"></i>
            </span>
            <span>Bienvenidos</span>
          </a>
        </li>
        <li>
          <a href="#">
            <span class="icon is-small">
              <i class="fas fa-home" aria-hidden="true"></i>
            </span>
            <span>Citas</span>
          </a>
        </li>
      </ul>
    </nav>
    <hr>
    <p class="has-text-centered">
      <span class="tag is-primary"><i class="fas fa-hand-paper"></i></span> Estás en la sección "Citas", aquí podrás ver a todos nuestros tutores pares para poder agendar una cita, si es que la necesitas.
    </p>
  </div>

  <div class="columns is-multiline">
    @foreach($tutorParCita as $one)
    <div class="column is-one-third">
        <div class="card large">
            <div class="card-content">
                <div class="media">
                    <div class="media-left">
                        <figure class="image is-96x96">
                            <img src="../../images/{{$one->image}}" alt="Image">
                        </figure>
                    </div>
                    <div class="media-content">
                        <p class="title is-4 no-padding">{{$one->name}}</p>
                    </div>
                </div>
                <br>
                <hr>
                <div class="content">
                    {{$one->soyAlumno}}
                    <hr>
                    <p><span class="title is-5">Cuatrimestre: </span> {{$one->cuatrimestreAlumno}}</p>
                    <p><span class="title is-5">P.E: </span> {{$one->divisionAlumno}}</p>
                </div>
                <hr>
                @guest
                    <a href="#" class="button is-link is-fullwidth" title="Necesitas iniciar sesión..." disabled>Conocer a detalle...</a>
                @else
                    <a href="/citas/{{$one->id}}" class="button is-link is-fullwidth">Conocer a detalle...</a>
                @endguest
            </div>
        </div>
    </div>
    @endforeach
  </div>


</div>

@endsection
