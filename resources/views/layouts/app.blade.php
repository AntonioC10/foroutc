<!DOCTYPE html>
<html class="has-navbar-fixed-top">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Tutoría en pares</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.4/css/bulma.min.css">
    <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
  </head>
  <body>

  <!-- NavBar -->
  <nav class="navbar is-fixed-top is-primary" role="navigation" aria-label="main navigation">
    <div class="navbar-brand">
      <a class="navbar-item" href="{{ url('Bienvenidos') }}">
        <img src="https://bulma.io/images/bulma-logo.png" width="112" height="28">
      </a>

      <a role="button" class="navbar-burger burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
        <span aria-hidden="true"></span>
        <span aria-hidden="true"></span>
        <span aria-hidden="true"></span>
      </a>
    </div>
  </nav>

  <!-- Saludo -->
  <section class="hero is-primary">
    <!-- Hero content: will be in the middle -->
    <div class="hero-body">
      <div class="container has-text-centered">
        <h1 class="title">
          <img src="http://utcancun.edu.mx/wp-content/uploads/2016/06/1200px-LogoBIS-01.png" width="300" height="600" alt="Universidad Tecnológoca de Cancún">
        </h1>
        <h2 class="is-size-1">
          Plataforma de Tutoría en pares
        </h2>
      </div>
    </div>
  </section>

  <!-- Body -->
  @yield('body')
  <hr>
  <div class="container">

  </div>

  <!-- Footer -->
  <footer class="footer">
    <div class="content has-text-centered">
      <p>
        <strong>Plataforma de tutoría en pares</strong> por <a href="http://utcancun.edu.mx/">Universidad Tecnológica de Cancún</a>(R). 2019
      </p>
    </div>
  </footer>

  <script src="https://unpkg.com/bulma-modal-fx/dist/js/modal-fx.min.js"></script>
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
  <script async type="text/javascript" src="../js/bulma.js"></script>
  <script src="../js/wild.js"></script>

</body>
</html>
